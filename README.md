##### Parametric CAD Modeling An Analysis of Strategies for Design Reusability


https://www.sciencedirect.com/science/article/pii/S0010448516000051


<hr>


##### Resilient Modeling Strategy  - Richard Gebhard


https://community.plm.automation.siemens.com/siemensplm/attachments/siemensplm/solid-edge-tkb/159/


https://www.youtube.com/watch?v=QGj3hwtyZxQ


<hr>


##### Solid Edge Synchronous Technology


[Synchronous Technology pdf](/Solid-Edge-Synchronous-Technology.pdf)


<hr>


##### Master Modelling


https://solidworksworld.lanyonevents.com/2017/connect/sessionDetail.ww?SESSION_ID=5582


[Master Modelling presentation pdf](/5582_Kimbrell-5582_Biasotti.pdf)


<hr>


##### Horizontal Modelling


https://dezignstuff.com/master-model-part-5-best-practice-and-horizontal-modeling/


[Horizontal Modelling pdf](/Delphi-Burke.pdf)


<hr>


##### Sketch Skeleton Modelling


https://forum.solidworks.com/message/729015#729015


[Sketch Skeleton Part Modelling pdf](/The-Skeleton-Sketch-Part-Introduction-6-8-2017.pdf)